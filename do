#!/bin/bash
#
set -uo pipefail

if [[ $# -lt 3 ]]; then
  echo 'Need a query name with arguments as follows:'
  find queries -type f -executable \
  -exec echo -n ' * '  \; \
  -exec basename {} \; \
  -exec grep __ {}.json \;
  echo
  echo "Example: $0 Förnamn A 20"
  echo "         Get all first names beginning with A for"
  echo "         the last 20 years, count per year"
  exit 1
fi

QUERY=$1; shift
JSON=queries/$QUERY.json
VALUE=$1; shift
TIME=$1; shift
OUTD=$QUERY.data
OUTF="$OUTD/$VALUE"
TMPFILE="scb_data_$(date +'%s.%N')"
ENDP=$(queries/$QUERY $@)

if [[ -f $OUTF ]]; then
  echo "$OUTF exists, exiting"
  exit 1
fi

iconv -f utf-8 -t iso-8859-1 $JSON | \
  sed -e "s/__VALUE__/$VALUE/g" | \
  sed -e "s/__TIME__/$TIME/g" > $TMPFILE

# Create output directory
mkdir -p $OUTD

echo "Retreiving data for $QUERY: $VALUE"

# In case they suddenly starts supporting accept-charset headers, we
# specify latin1 here.
curl --fail --silent \
  -H "Content-Type: application/json" \
  -H "Accept-Charset: iso-8859-1;q=1" \
  --data @${TMPFILE} \
  $ENDP > $OUTF

if [[ $? -ne 0 ]]; then
  echo "No result (or too many) for $VALUE"
  rm $OUTF
else
  # Remove BOM etc
  dos2unix -q $OUTF

  # If the result was json, convert it to csv.
  if grep -q json $TMPFILE; then
    ./json2csv.php $OUTF > $TMPFILE
    mv $TMPFILE $OUTF
  elif grep -q csv $TMPFILE; then
    iconv -t utf-8 -f iso-8859-1 $OUTF > $TMPFILE
    mv $TMPFILE $OUTF
  fi

fi

rm -f $TMPFILE
