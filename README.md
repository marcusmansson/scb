README
======

Linux tools for grabbing name data from Statistiska centralbyrån (SCB).

This was developed because the SCB website makes retrieval of huge
datasets impossible or at least very impractical. Luckily they have an 
API for doing it without silly disfunctional forms, but see the
caveats below.

Included is also *all swedish name data for the last 20 years* in CSV format.


Caveats for Statistikdatabasen (SCB)
------------------------------------

[API handbook](https://www.scb.se/contentassets/79c32c72783a4f67b202ad3189f921b9/api-beskrivning.pdf)

Things worth mentioning but not mentioned in the handbook:

* The example URLs in the manual are wrong.
* The API fails to read input in UTF-8 (åäö).
* The API fails to deliver CSV in UTF-8.
* The API delivers JSON in UTF-8 but with BOM, which is bad:
  [RFC 7159](https://tools.ietf.org/html/rfc7159#section-8.1) clearly states that:
  > Implementations MUST NOT add a byte order mark to the beginning of a JSON text.
* When requesting output in the formats XLSX and CSV you don't get the Name[KM] format,
  but "Name[ ]?". For json, json-stat, SDMX the "forname" value is of type Name[KM].
* A whitespace trailing name does not indicate if women or men are bearing the name,
  as we can tell from the example below.

  Why not use NameKM syntax, as in the other output formats?

  Fetching all fornames that begin with A for the last 20 years as CSV from
  the API gives the confusing result (excerpt):

  ```
  "Abdulatif",..,..,..,..,..,..,..,..,..,..,..,..,..,..,..,..,..,..,10,..
  "Abdulatif ",..,..,..,..,..,..,..,..,..,11,12,14,13,16,20,28,32,41,46,51
  "Abdulazeez ",..,..,..,..,..,..,..,..,..,20,32,38,43,46,45,46,48,49,52,53
  "Abdulazeez",..,..,..,..,..,..,..,..,..,25,38,44,45,46,50,48,49,54,58,61
  ```
  Fetching the same data as json (and converting it to CSV yourself) yields:

  ```
  "AbdulatifK",..,..,..,..,..,..,..,..,..,..,..,..,..,..,..,..,..,..,10,..
  "AbdulatifM",..,..,..,..,..,..,..,..,..,11,12,14,13,16,20,28,32,41,46,51
  "AbdulazeezK",..,..,..,..,..,..,..,..,..,20,32,38,43,46,45,46,48,49,52,53
  "AbdulazeezM",..,..,..,..,..,..,..,..,..,25,38,44,45,46,50,48,49,54,58,61
  ```

  The latter being much clearer.

* Searches are case sensitive, so a != A.
* There's a Server Fail message if the dataset returned would be too large (more
  than 25000 records or so it seems), only tested this with CSV though.
* Error messages are in HTML format, not json or whatever the client asked for.
* The web site breaks when the backend fails. Try [selecting all names here](http://www.statistikdatabasen.scb.se/pxweb/sv/ssd/START__BE__BE0001__BE0001G/BE0001ENamn10/?rxid=615fa9df-f9a2-40d8-9689-e6fadc6bc955) for instance.
* The Server Error message does not describe the problem at hand when using the API:
    * 403 - Access denied, ought instead be 413 - Payload Too Large
    * 404 - File not found, should maybe be 204 - No Content
* Generally the handbook needs more examples, for instance command line interactions:

```bash
  curl -H "Content-Type: application/json" \
  --data '{"query":[{"code":"Fornamn","selection":{"filter":"all","values":["Ab*"]}},{"code":"Tid","selection":{"filter":"top","values":["1"]}}],"response":{"format":"csv"}}' \
  http://api.scb.se/OV0104/v1/doris/sv/ssd/BE/BE0001/BE0001G/BE0001FNamn10
```
