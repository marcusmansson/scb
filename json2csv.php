#!/usr/bin/env php
<?php
/*
 * @file
 * Convert json result to csv (for this application, not generally).
 *
 */
try {
  $data = init($argv);
  parse_data($data);
}
catch (Exception $error) {
  errlog($error->getMessage());
  exit(1);
}

function errlog($message = '') {
  fwrite(STDERR, $message . PHP_EOL);
}

function init(&$argv) {
  // If there is anything on standard input use
  // that as data source.
  stream_set_blocking(STDIN, 0);
  $json = fgets(STDIN);

  if (empty($json) && empty($argv[1])) {
     // Nothing on stdin and no args, user needs help!
     throw new Exception("Usage: {$argv[0]} [file|STDIN]");
  }
  else if (empty($json)) {
    // Nothing on stdin but an argument was given,
    // use that file as data source.
    $file = $argv[1];

    if (!is_file($file)) {
      throw new Exception("$file is not a file");
    }

    $json = file_get_contents($file);
  }

  if (! $data = json_decode($json)) {
    throw new Exception("could not decode input, is it really json?");
  }

  return $data;
}

function parse_data(&$data) {
  if (! (isset($data->columns) && isset($data->data))) {
    throw new Exception("incompatible data format");
  }

  $axis = array();

  foreach ($data->columns as $i => $o) {
    if ($o->type == 'c')
      break;

    $axis[$i] = array(
      'text' => $o->text,
      'values' => array(),
    );
  }

  foreach ($data->data as $index => $o) {
    $x = $o->key[0]; // item
    $y = $o->key[1]; // year
    $value = $o->values[0];

    if (empty($axis[1]['values'][$y])) {
      $axis[1]['values'][$y] = intval($value);
    }
    else {
      $axis[1]['values'][$y] += intval($value);
    }

    if (empty($axis[0]['values'][$x])) {
      $axis[0]['values'][$x] = array();
      $target =& $axis[0]['values'][$x];
    }

    $target[] = $value;
  }

  // Output headers.
  fputcsv(STDOUT, array_merge(
    (array)$axis[0]['text'],
    array_keys($axis[1]['values'])
  ));

  // Output rows.
  foreach ($axis[0]['values'] as $key => $values) {
    fputcsv(STDOUT, array_merge((array)$key, $values));
  }
}
